package com.company;

import java.awt.*;

public class Grid {
    private Cell[][] grid = new Cell[20][20];
    private static final int MARGIN = 10;
    private static final int WIDTH = 35;
    private static final int HEIGHT = 35;

    public Grid() {
        for (int i = 0; i < grid.length; i++) {
            for (int k = 0; k < grid.length; k++) {
                grid[i][k] = new Cell(MARGIN + i * WIDTH, MARGIN + k * HEIGHT);

            }
        }
    }

    public void paint(Graphics g, Point mousePosition) {
        for (int i = 0; i < grid.length; i++) {
            for (int k = 0; k < grid.length; k++) {
                grid[i][k].paint(g, grid[i][k].contains(mousePosition));

            }
        }
    }
}






